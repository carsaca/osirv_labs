# Napisati program koji će učitati sliku i spremiti tri odvojene slike,
# po jednu za svaki kanal. Nazvati ih crvena.jpg, plava.jpg i zelena.jpg.
# (Hint: Pri snimanju jednog kanala, postaviti druga dva kanala na 0)

import numpy as np 
import cv2

slika = cv2.imread('../images/baboon.bmp')

plava = slika.copy()
plava[:,:,1:3] = 0

zelena = slika.copy()
zelena[:,:,::2] = 0

crvena = slika.copy()
crvena[:,:,0:2] = 0

cv2.imwrite("plava.jpg", plava)
cv2.imwrite("zelena.jpg", zelena)
cv2.imwrite("crvena.jpg", crvena)


