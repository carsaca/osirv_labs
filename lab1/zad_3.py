# Napisati program koji će učitati sliku i napraviti oko učitane slike obrub debljine 10 piksela.
# Takvu sliku spremiti.

import numpy as np 
import cv2

def show(title, image):
    cv2.imshow(title,image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

slika = cv2.imread('../images/lenna.bmp')

nova_slika = cv2.copyMakeBorder(slika,10,10,10,10,cv2.BORDER_CONSTANT,1)

#show("test",nova_slika)
cv2.imwrite("lennaObrub.jpg", nova_slika)