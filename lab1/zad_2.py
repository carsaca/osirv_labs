# Isti zadatak kao 1., ali kanale prikazati na ekranu koristeći imshow().

import numpy as np 
import cv2

def show(title, image):
    cv2.imshow(title,image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


slika = cv2.imread('../images/pepper.bmp')

plava = slika.copy()
plava[:,:,1:3] = 0

zelena = slika.copy()
zelena[:,:,::2] = 0

crvena = slika.copy()
crvena[:,:,0:2] = 0

show("plava.jpg", plava)
show("zelena.jpg", zelena)
show("crvena.jpg", crvena)
