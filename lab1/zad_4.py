# Napisati program koji će učitati sliku, te iz te slike napraviti tri slike.
# Prva će imati dvostruko manje vertikalnih piksela od originala (redova),
# druga će imati dvostruko manje horizontalnih piksela od originala (stupaca),
# a treća će imati dvostruko manje i jednih i drugih. (Hint: uzimate svaki drugi piksel)

import numpy as np 
import cv2

def show(title, image):
    cv2.imshow(title,image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

slika = cv2.imread('../images/airplane.bmp')
slika_size = np.size(slika)

uska_slika = slika[:,::2].copy()
spljostrena_slika = slika[::2,:].copy()
mala_slika = slika[::2,::2].copy()

show("test", uska_slika)
#show("test", spljostrena_slika)
#show("test", mala_slika)

cv2.imwrite("uska_slika.jpg", uska_slika)
cv2.imwrite("spljostrena_slika.jpg", spljostrena_slika)
cv2.imwrite("mala_slika.jpg", mala_slika)