import numpy as np
import argparse
import glob
import cv2
from matplotlib import pyplot as plt

def auto_canny(image, sigma=0.33):
        # compute the median of the single channel pixel intensities
        v = np.median(image)

        # apply automatic Canny edge detection using the computed median
        lower = int(max(0, (1.0 - sigma) * v))
        upper = int(min(255, (1.0 + sigma) * v))
        edged = cv2.Canny(image, lower, upper)
        print ("Median lower: %r." % lower)
        print ("Median upper: %r." % upper)
        # return the edged image
        return edged, lower, upper

images = ["airplane.bmp", "barbara.bmp", "boats.bmp", "pepper.bmp"]

for i in images:
    image = cv2.imread("../../slike/" + i ,0)
    print(i)
    auto, l, u = auto_canny(image)
    cv2.imwrite(i + "_" + str(l) + "-" + str(u) + ".png", auto)

# plt.subplot(121),plt.imshow(image, cmap='gray')
# plt.title('Original Image'), plt.xticks([]), plt.yticks([])
# plt.subplot(122),plt.imshow(auto,  'gray')
# plt.title('Edges'), plt.xticks([]), plt.yticks([])
# plt.show()
