# LV5


## Zadatak 1

lower=0, upper=255
![lower=0, upper=255](problem_1/canny_0-255.png)

lower=128, upper=128
![lower=128, upper=128](problem_1/canny_128-128.png)

lower=255, upper=255
![lower=255, upper=255](problem_1/canyy_255-255.png)

Najbolja je verzija 255-255


## Zadatak 2

airplane.bmp
Median lower: 134.
Median upper: 255.
![airplane](problem_2/airplane.bmp_134-255.png)


barbara.bmp
Median lower: 72.
Median upper: 143.
![barbara](problem_2/barbara.bmp_72-143.png)


boats.bmp
Median lower: 92.
Median upper: 183.
![boats](problem_2/boats.bmp_92-183.png)


pepper.bmp
Median lower: 81.
Median upper: 160.
![pepper](problem_2/pepper.bmp_81-160.png)



## Zadatak 3

theta= 90, threshold = 150 
![theta= 90, threshold = 150](problem_3/chess_90-150.png)


theta=180, threshold = 200 
![theta=180, threshold = 200](problem_3/chess_180-200.png)


theta= 90, threshold = 200 
![theta= 90, threshold = 200](problem_3/chess_90-200.png)


theta=180, threshold = 150
![theta=180, threshold = 150](problem_3/chess_180-150.png)


Najbolji je, po mom mišljenju, 3. rezultat, theta = 90, threshold = 200.


## Zadatak 4

blocksize = 1
![blocksize = 1](problem_4/chess_1.png)

blocksize = 3
![blocksize = 3](problem_4/chess_3.png)

blocksize = 5
![blocksize = 5](problem_4/chess_5.png)


## Zadatak 5

Keypoints
![keypoints](problem_5/Keypoints.png)

Matches
![matches](problem_5/Matches.png)