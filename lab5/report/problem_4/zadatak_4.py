import cv2
import numpy as np
from matplotlib import pyplot as plt
import copy

img = cv2.imread('../../slike/chess.jpg')
img2 = copy.copy(img)
img2 = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)

gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
gray = np.float32(gray)

values = [1,3,5]
for i in values:
    dst = cv2.cornerHarris(gray,i,3,0.04)

    #result is dilated for marking the corners, not important
    dst = cv2.dilate(dst,None)

    # threshold for an optimal value, it may vary depending on the image.
    img2[dst>0.01*dst.max()]=[0,0,255]

    cv2.imwrite("chess_" + str(i) + ".png", cv2.cvtColor(img2, cv2.COLOR_BGR2RGB))
