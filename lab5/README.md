# Lab 5 - Feature Detection (Edge Detection, Hough Transform, Corner Detection) and Feature Extraction


<h3>Exercises</h3>

You need to create the `report` directory and the file `README.md` inside of it.  <br>
`README.md` file should contain Your task-solving and excersise reports.



<h2>5.1. Edge Detection</h2>

When talking about images, the edge can be defined as a boundary between two regions with relatively distinct gray level properties.
Edges are pixels where the brightness function changes abruptly.
Edge detectors are a collection of very important local image preprocessing methods used to locate (sharpen) changes in the intensity function.
Different edge detection methods include Canny, Sobel, Roberts, SUSAN, Prewitt, and Deriche.

<h3>5.1.1. Edge Detection with Canny Edge descriptor</h3>

Canny Edge Detection is a popular edge detection algorithm. 
This is a multi-step algorithm, so its successful performance, depends on few steps: 

<ul>
  <li><b>Preprocessing (noise reduction)</b></li>
  Since all edge detection results are easily affected by image noise, it is essential to filter out the noise to prevent false detection caused by noise. 
  To smooth the image, we will use a Gaussian filter. 
  This step will slightly smooth the image to reduce the effects of obvious noise on the edge detector.
  The equation for a Gaussian filter with a kernel of size (2k+1) x (2k+1) is given with: 
 <p align="center">
 ![gauss](https://wikimedia.org/api/rest_v1/media/math/render/svg/4a36d7f727beeaff58352d671bb41a3aca9f44d6)
 </p>
  
  As seen from the above formula, important parameters for the Gaussian filter are:
  <ul>
    <li>size of the kernel (mostly 5x5 kernel is used)</li>
    <li>and standard deviation sigma </li>
  </ul>
  
   <p>
  <li><b>Finding intensity gradient of the image</b></li>
  An edge in an image may point in a variety of directions, so the Canny algorithm uses filters to detect horizontal, vertical and diagonal edges in the blurred image. 
  The edge detection operator (such as Roberts, Prewitt, or Sobel) returns a value for the first derivative in the horizontal direction (Gx) and the vertical direction (Gy). 
  In other words, <i>the magnitude</i> of the gradient at a point in the image determines if it possibly lies on the edge or not. A high gradient magnitude means the colors are changing rapidly which implies the existence of an edge while a low gradient implies that edge is not present. 
  The <i>direction</i> of the gradient shows how the edge is oriented.
  To calculate these, following formulas are used: 

<p align="center">
 ![canny-edge-1](http://latex.codecogs.com/gif.latex?Edge%20Gradient%20%28G%29%20%3D%20%5Csqrt%7BG_x%5E2&amp;plus;G_y%5E2%7D)
 </p>
 <p align="center">
![canny-edge-2](http://latex.codecogs.com/gif.latex?Angle%20%28%5Ctheta%29%20%3D%20%5Ctan%5E%7B-1%7D%5Cfrac%7BG_y%7D%7BG_x%7D)
 </p>

  Once we have the gradient magnitudes and orientations, we can get started with the actual edge detection.
 
 </p>

 <p>
  <li><b>Applying non-maximum suppression to get rid of spurious response to the edge detection</b></li>

  After gradient magnitude and direction are obtained, a full scan of the image is done to remove any unwanted pixels which may not constitute the edge.
  Therefore, edge thining technique known as non-maximum suppression is applied to find all those unwanted pixels. 
  For this, at every pixel, the pixel is checked if it is a local maximum in its neighborhood in the direction of the gradient. Check the image below: 

 <p align="center">
   ![non-maximum](https://docs.opencv.org/3.1.0/nms.jpg)

  </p>
  <br>
  Point A is on the edge ( in the vertical direction). Gradient direction is normal to the edge. Point B and C are in gradient directions. 
  So point A is checked with point B and C to see if it forms a local maximum. If so, it is considered for the next stage, otherwise, it is suppressed ( put to zero).
  In short, the result you get is a binary image with "thin edges".
</p>

  <li><b>Track edge by hysteresis </li></b>
  Finalize the detection of edges by suppressing all the other edges that are weak and not connected to strong edges.
  This stage decides which are all edges are really edges and which are not. For this, we need two threshold values, minVal, and maxVal. 
  Any edges with intensity gradient more than maxVal are sure to be edges and those below minVal are sure to be non-edges, so discarded. 
  Those who lie between these two thresholds are classified edges or non-edges based on their connectivity. 
  If they are connected to "sure-edge" pixels, they are considered to be part of edges. Otherwise, they are also discarded. See the image below:
<p align="center">
    ![non-maximum](https://docs.opencv.org/3.1.0/hysteresis.jpg)
  
 </p>
 <p>
    The edge A is above the maxVal, so considered as "sure-edge". Although edge C is below maxVal, it is connected to edge A, so that also considered as valid edge and we get that full curve. 
    But edge B, although it is above minVal and is in the same region as that of edge C, it is not connected to any "sure-edge", so that is discarded. 
    So it is very important that we have to select minVal and maxVal accordingly to get the correct result. 
    This stage also removes small pixels noises on the assumption that edges are long lines. So what we finally get is strong edges in the image.
 </p>
  
</ul>

<hr />

<h4>Excersise 1</h4>

OpenCV implementation of Canny edge detector:

```
Function : cv2.Canny(blurred image,lower threshold,upper threshold)
Parameters are as follows :
1. blurred image : input image blurred with Gaussian 5 by 5 kernel
2. lower threshold : first threshold for the hysteresis procedure
3. upper threshold : second threshold for the hysteresis procedure
```
More information can be found at:  <a href="https://docs.opencv.org/3.0-beta/modules/imgproc/doc/feature_detection.html?highlight=cv2.canny#cv2.Canny">OpenCV cv2.Canny documentation</a> 


<b>Try out one of the following codes in order to perform Canny Edge detection on image `lenna.bmp`. </b>

```
import cv2
import numpy as np
from matplotlib import pyplot as plt

img = cv2.imread('../slike/lenna.bmp',0)
blurred = cv2.GaussianBlur(img, (5,5), 0)
edges = cv2.Canny(blurred,255,255)

plt.subplot(121),plt.imshow(img, cmap='gray')
plt.title('Grayscale Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(edges,cmap = 'gray')
plt.title('Edge Image'), plt.xticks([]), plt.yticks([])
plt.show()

```

Example of Canny Edge detection code with trackball to ease the tunning process:  

```
import cv2
import numpy as np
from matplotlib import pyplot as plt

# this function creates trackbar 
def nothing(x):
    pass

# read the image
img = cv2.imread('../slike/lenna.bmp', 0)

# create trackbar for canny edge detection threshold changes
cv2.namedWindow('canny')

# add ON/OFF switch to "canny"
switch = '0 : OFF \n1 : ON'
cv2.createTrackbar(switch, 'canny', 0, 1, nothing)

# add lower and upper threshold slidebars to "canny"
cv2.createTrackbar('lower', 'canny', 0, 255, nothing)
cv2.createTrackbar('upper', 'canny', 0, 255, nothing)

# infinite loop until we hit the escape key on keyboard
while(1):

    # get current positions of four trackbars
    lower = cv2.getTrackbarPos('lower', 'canny')
    upper = cv2.getTrackbarPos('upper', 'canny')
    s = cv2.getTrackbarPos(switch, 'canny')

    if s == 0:
        edges = img
    else:
        edges = cv2.Canny(img, lower, upper)

#   display images
    cv2.imshow('original', img)
    cv2.imshow('canny', edges)


    k = cv2.waitKey(1) & 0xFF
    if k == 27:   # hit escape to quit
        break

cv2.destroyAllWindows()

```
<h4>Task 1</h4>
Try different value combinations for lower and upper thresholds and save resultant images. 
<ul>
<li>lower=0, upper=255</li>
<li>lower=128, upper=128</li>
<li>lower=255, upper=255</li>
</ul>

How does change in threshold values affects edge detection?  
In your opinion what thresholds gave best results?

<hr />

As you could observe, the common problem here is in determining these lower and upper thresholds. So, what is the optimal value for the thresholds?
This question is especially important when you are processing multiple images with different contents captured under varying lighting conditions.  
A little trick that relies on basic statistics can help you automatically determ these values, removing the manual tuning of the thresholds for Canny edge detection.

<h4>Excersise 2</h4>

<b>Try out the following code in order to perform statistically based automatic Canny Edge detection on image `lenna.bmp`. </b>

```
import numpy as np
import argparse
import glob
import cv2
from matplotlib import pyplot as plt

def auto_canny(image, sigma=0.33):
        # compute the median of the single channel pixel intensities
        v = np.median(image)

        # apply automatic Canny edge detection using the computed median
        lower = int(max(0, (1.0 - sigma) * v))
        upper = int(min(255, (1.0 + sigma) * v))
        edged = cv2.Canny(image, lower, upper)
        print "Median lower: %r." % lower
        print "Median upper: %r." % upper
        # return the edged image
        return edged

image = cv2.imread("../slike/lenna.bmp",0)
auto = auto_canny(image)

plt.subplot(121),plt.imshow(image, cmap='gray')
plt.title('Original Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(auto,  'gray')
plt.title('Edges'), plt.xticks([]), plt.yticks([])
plt.show()

```

<h4>Task 2</h4>

Perform automatic Canny edge detection on images: `airplane.bmp` , `barbara.bmp` , `boats.bmp` , `pepper.bmp` .
Write down obtained median thresholds and save result images.   

<ul>
<li>lower=0, upper=255</li>
<li>lower=128, upper=128</li>
<li>lower=255, upper=255</li>
</ul>

<hr />

<h3>5.2. Hough Transformation for Lines</h3>

When images are to be used in different areas of image analysis such as object recognition, it is important to reduce the amount of data in the image while preserving the important,
characteristic, structural information. Edge detection makes it possible to reduce the amount of data in an image considerably. However the output from an edge detector is still a image
described by it’s pixels. If lines, ellipses and so forth could be defined by their characteristic equations, the amount of data would be reduced even more. 
The Hough transform was originally developed to recognize lines and has later been generalized to cover arbitrary shapes. 

<ul>
<li> <b>Representation of Lines in the Hough Space</b> </li>

Lines can be represented uniquely by with parameters a and b and following equation: 
<p align="center">
![line-1](https://latex.codecogs.com/gif.latex?y%3Da%5Ccdot%20x%20&plus;b)
</p>
Above equation is not able to represent vertical lines. Therefore, the Hough transform uses the following equation: <br>
<p align="center">
![line-2](https://latex.codecogs.com/gif.latex?r%20%3D%20x%20%5Ccdot%20%5Ccos%20%5CTheta%20&plus;%20y%5Ccdot%20%5Csin%20%5CTheta)
</p>
To obtain similar equation to the first one, this can be rewritten as: 
<p align="center">
![line-3](https://latex.codecogs.com/gif.latex?y%20%3D%20-%20%5Cfrac%7B%5Ccos%20%5CTheta%20%7D%7B%5Csin%20%5CTheta%20%7D%20%5Ccdot%20x%20&plus;%20%5Cfrac%7Br%7D%7B%5CTheta%20%7D)
</p>
The parameters ![line-1](https://latex.codecogs.com/gif.latex?%5Ctheta) and r is the angle of the line and the distance from the line to the origin, respectively. 
All lines can be represented in this form when  <img src="https://i.ibb.co/4Sq4DkD/formula1.png" alt="hough1" border="0"></a>.
To sum up, the Hough space for lines has these two dimensions: ![line-1](https://latex.codecogs.com/gif.latex?%5Ctheta) and r and a line is represented
by a single point, corresponding to a unique set of parameters ![line-1](https://latex.codecogs.com/gif.latex?%28%5Ctheta%20_%7B0%7D%2Cr_%7B0%7D%29) . 
The line-to-point mapping is illustrated in the following image: 
<p align="center">
<img src="https://i.ibb.co/gJ9C8Jy/hough1.png" alt="hough1" border="0"></a><br /><br />
</p>

<li> <b>Mapping of edge points to the Hough space</b> </li>

An important concept for the Hough transform is the mapping of single points. The idea is, that a point is mapped to all lines, that can pass through that point.
This yields a sine-like line in the Hough space.  This principle is illustrated for a point ![formula](https://latex.codecogs.com/gif.latex?p_%7B0%7D%3D%20%2840%2C30%29)as shown in following figures: 
<p align="center">
<img src="https://i.ibb.co/GVRDyJs/houghspace.png" alt="houghspace" border="0">
</p>
On the left image transformation of a single point &nbsp; ![sinle_point](https://latex.codecogs.com/gif.latex?p_%7B0%7D) &nbsp; to a line in the Hough space is shown while on the right image 
the Hough space line representation through all possible lines through &nbsp; ![sinle_point](https://latex.codecogs.com/gif.latex?p_%7B0%7D) &nbsp; is shown. 

<li><b>The Hough Space Accumulator<b/></li>
To determine the areas where most Hough space lines intersect, an accumulator covering the Hough space is used. When an edge point is transformed, bins in the accumulator is incremented
for all lines that could pass through that point. The resolution of the accumulator determines the precision with which lines can be detected. 
In general, the number of dimensions of the accumulator corresponds to the number of unknown parameters in the Hough transform problem. Thus, for ellipses a 5-dimensional space is required
(the coordinates of its center, the length of its major and minor axis, and its angle). For lines 2 dimensions suffice (r and θ). This is why it is possible to visualize the content of the ac
cumulator.


<li> <b>Detection of Infinite Lines</b> </li>

Infinite lines are detected by interpretation of the accumulator when all edge points has been transformed. The most basic way the detect lines is to set some threshold for the accumulator, and interpret
all values above the threshold as a line. The threshold could for instance be 50% of the largest value in the accumulator. 

</ul>


To sum up, the algorithm for detecting straight lines can be divided to the following steps: 
<ul>
<li> Edge detection, e.g. using the Canny edge detector </li>
<li> Mapping of edge points to the Hough space and storage in an accumulator </li>
<li> Interpretation of the accumulator to yield lines of infinite length. The interpretation isdone by thresholding and possibly other constraints. </li>
<li> Conversion of infinite lines to finite lines. </li>
</ul>

More information about Hough Transformation for lines can be found at:  <a href="href> https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_imgproc/py_houghlines/py_houghlines.html">OpenCV Hough Transform documentation</a> 

<h4>Excersise 3</h4>

OpenCV implementation of Hough Transform for lines can be found in: 

```
Function : cv2.HoughLines(image with edges,rho,theta,threshold, array,srn,stn)
Parameters are as follows :
1. image with edges : input image with found edges
2. rho : distance resolution of the accumulator in pixels
3. theta : angle resolution of the accumulator in radians
4. threshold: accumulator threshold parameter. only those lines are returned that get enough votes 
5. array: return an empty array with shape and type of input for storing result
6. srn: for the multi-scale Hough transform, if 0 standard Hough transform is used
7. stn: for the multi-scale Hough transform, if 0 standard Hough transform is used
```
More information can be found at:  <a href="https://docs.opencv.org/3.0-beta/modules/imgproc/doc/feature_detection.html?highlight=cv2.hough#cv2.HoughLines">OpenCV cv2.HoughLines documentation</a> 

<b>Try out the following code in order to find lines on the image `chess.jpg`. </b>

```
import cv2
import numpy as np
import math
import copy
from matplotlib import pyplot as plt

img = cv2.imread('../slike/chess.jpg')
img2 = copy.copy(img)
img2 = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
gray=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
edges = cv2.Canny(gray, 166, 255)

lines= cv2.HoughLines(edges, 1, math.pi/90, 200, np.array([]), 0, 0)

a,b,c = lines.shape
for i in range(a):
    rho = lines[i][0][0]
    theta = lines[i][0][1]
    a = math.cos(theta)
    b = math.sin(theta)
    x0, y0 = a*rho, b*rho
    pt1 = ( int(x0+1000*(-b)), int(y0+1000*(a)) )
    pt2 = ( int(x0-1000*(-b)), int(y0-1000*(a)) )
    cv2.line(img2, pt1, pt2, (255, 0, 0), 2, cv2.LINE_AA)

plt.subplot(121),plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
plt.title('Original Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(img2, 'gray')
plt.title('Detected Lines'), plt.xticks([]), plt.yticks([])
plt.show()

```
<h4>Task 3</h4>

Find lines with Hough transform on the image: `chess.jpg`. 
Change theta and threshold parameters to the following values:
<ul>
<li>theta= 90, threshold = 150 </li>
<li>theta=180, threshold = 200 </li>
<li>theta= 90, threshold = 200 </li>
<li>theta=180, threshold = 150 </li>
</ul>

Save result images. 
What threshold gave best results? 

<hr />

<h4>5.3. Corner Detection</h4>

Corners are locations in images where a slight shift in the location will lead to a large change in intensity in both horizontal and vertical axes.
The Harris Corner detection algorithm consist of following steps:

<ul>
<li><b>Determination of windows (small image patches) with large intensity variation</b></li>

Let a window (the center) be located at position  &nbsp; ![cornerr](https://latex.codecogs.com/gif.latex?%28x%2Cy%29).  &nbsp; 
Let the intensity of the pixel at this location be  &nbsp; ![cornerrr](https://latex.codecogs.com/gif.latex?I%28x%2Cy%29).  &nbsp;
If this window slightly shifts to a new location with displacement  &nbsp; ![corner7](https://latex.codecogs.com/gif.latex?%28u%2Cv%29),  &nbsp; the intensity of the pixel at this location will be 
 &nbsp; ![corner8](https://latex.codecogs.com/gif.latex?I%28x&plus;u%2Cy&plus;v%29).  &nbsp; 

Therefore,  &nbsp; ![corner9](https://latex.codecogs.com/gif.latex?%5BI%28x&plus;u%2Cy&plus;v%29-I%28x%2Cy%29%5D)  &nbsp; will be the difference in intensities of the window shift. 
For a corner, this difference will be very high. 
We maximize this term by differentiating it with respect to the X and Y axes. 
Let  &nbsp; ![corner10](https://latex.codecogs.com/gif.latex?w%28x%2Cy%29)  &nbsp;be the weights of pixels over a window (Rectangular or a Gaussian).
Then,  &nbsp; ![corner11](https://latex.codecogs.com/gif.latex?E%28u%2Cv%29)  &nbsp; is defined as :
<p align="center">
<img src="https://cdn-images-1.medium.com/max/800/0*v4pgxvEFE8JvroJv.png" alt="hough1" border="0"></a><br /><br />
</p>


Since,computing &nbsp; ![corner11](https://latex.codecogs.com/gif.latex?E%28u%2Cv%29)  &nbsp; will be computationally challenged, optimisation with Taylor series expansion (only the 1rst order)
is applyed. Some math leads us to: <br/>
<p align="center">
&nbsp;![corner12](https://latex.codecogs.com/gif.latex?E%28u%2Cv%29%5Capprox%20%28u%2Cv%29M%5Cbinom%7Bx%7D%7By%7D). &nbsp; 
</p>
And finally structure tensor is defined with :
<p align="center">
&nbsp;<img src="https://wikimedia.org/api/rest_v1/media/math/render/svg/a617dda21e306dbfbdb7a186b1c203e3f3443867" alt="hough1" border="0"></a><br /><br />
</p>

<li><b>Computation of score R for each found window</b></li>
After fiding windows with large variations, selection of suitable corners is performed. 
It was estimated that the eigenvalues of the matrix can be used to do this. Calculation of a score associated with each such window is given with:  <br/>
<p align="center">
![corner2](https://latex.codecogs.com/gif.latex?R%20%3D%20det%28M%29%20-%20k%5Ccdot%20%28trace%28M%29%29%5E%7B2%7D)   <br/>
</p>
where &nbsp; ![corner2](https://latex.codecogs.com/gif.latex?det%28M%29%20%3D%20%5Clambda%20_%7B1%7D%20%5Ccdot%20%5Clambda_%7B2%7D) &nbsp; and &nbsp; 
&nbsp; ![corner3](https://latex.codecogs.com/gif.latex?trace%28M%29%20%3D%20%5Clambda%20_%7B1%7D%20&plus;%20%5Clambda_%7B2%7D).  &nbsp;
Here,  &nbsp; ![corner4](https://latex.codecogs.com/gif.latex?%5Clambda%20_%7B1%7D) &nbsp; and  &nbsp; ![corner5](https://latex.codecogs.com/gif.latex?%5Clambda%20_%7B2%7D)  &nbsp; 
are eigenvalues of M, and k is an empirical constant. 


<li><b>Applying a threshold to the score R and important corners selection<b/></li>

Depending on the value of R, the window is classified as consisting of flat, edge, or a corner. 
A large value of R indicates a corner, a negative value indicates an edge. 
Also, in order to pick up the optimal values to indicate corners, we find the local maxima as corners within the window which is a 3 by 3 filter.

</ul>



<h4>Excersise 4</h4>

OpenCV implementation of Harris Corner detector can be found in: 

```
Function : cv2.cornerHarris(image,blocksize,ksize,k)
Parameters are as follows :
1. image : the source image in which we wish to find the corners (grayscale)
2. blocksize : size of the neighborhood in which we compare the gradient 
3. ksize : aperture parameter for the Sobel() Operator (used for finding Ix and Iy)
4. k : Harris detector empirical constant parameter (used in the calculation of R)
```
More information can be found at:  <a href="https://docs.opencv.org/3.0-beta/modules/imgproc/doc/feature_detection.html?highlight=cv2.cornerharris#cv2.cornerHarris">OpenCV cv2.cornerHarris documentation</a> 

<b>Try out the following code in order to find corners on the image `chess.jpg`. </b>

```
import cv2
import numpy as np
from matplotlib import pyplot as plt
import copy

img = cv2.imread('../slike/chess.jpg')
img2 = copy.copy(img)
img2 = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)

gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

gray = np.float32(gray)
dst = cv2.cornerHarris(gray,2,3,0.04)

#result is dilated for marking the corners, not important
dst = cv2.dilate(dst,None)

# threshold for an optimal value, it may vary depending on the image.
img2[dst>0.01*dst.max()]=[0,0,255]

plt.subplot(121),plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
plt.title('Original Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(img2 , 'gray')
plt.title('Detected Corners'), plt.xticks([]), plt.yticks([])
plt.show()

```
<h4>Task 4</h4>

Perform Harris Corner detection on image `chess.jpg` .
Change size of the neighborhood values as follows:

<ul>
<li>blocksize=1</li>
<li>blocksize=3</li>
<li>blocksize=5</li>

</ul>

Save all result images and compare obtained results. 

<hr />


<h3>5.4. Feature Detection with ORB detector </h3>

What is feature? 
A local image feature is a tiny patch in the image that's invariant to image scaling, rotation and change in illumination. 
It's like the tip of a tower, or the corner of a window in the image above. Unlike a random point on the background (sky) in the image above, 
the tip of the tower can be precise detected in most images of the same scene. It is geometricly (translation, rotation, ...) and photometricly (brightness, exposure, ...) invariant.

A good local feature is like the piece you start with when solving a jigsaw puzzle, except on a much smaller scale. 
It's the eye of the cat or the corner of the table, not a piece on a blank wall.

The extracted local features must be:
<ul>
<li>Repeatable and precise so they can be extracted from different images showing the same object.</li>
<li>Distinctive to the image, so images with different structure will not have them.</li>
</ul>

Due to these requirements, most local feature detectors extract corners and blobs.
There is a wealth of algorithms satisfying the above requirements for feature detection (finding interest points on an image) and description 
(generating a vector representation for them). They include already learned Harris Corner Detection, Scale Invariant Feature Transform (SIFT), 
Speeded-Up Robust Features (SURF), Features from Accelerated Segment Test (FAST), and Binary Robust Independent Elementary Features (BRIEF)


Oriented FAST and rotated BRIEF (ORB) is a fast robust local feature detector. It is basically a fusion of FAST keypoint detector and BRIEF descriptor
with many modifications to enhance the performance. 


ORB is a good choice in low-power devices for panorama stitching etc.

<h4>Excersise 5</h4>

OpenCV implementation of ORB detector:  

```
Function : cv2.ORB_create()
Creates object for ORB detector. 
```
More information can be found at:  <a href="https://docs.opencv.org/3.0-beta/modules/features2d/doc/feature_detection_and_description.html">OpenCV ORB detector documentation</a> 

<b>Try out the following code in order to find corners on the image `building_1.jpg`. </b>


```
import numpy as np
import pandas as pd
import cv2
import matplotlib.pyplot as plt
import os

# Features Detection: 
dataset_path = '.'
img = cv2.imread(os.path.join(dataset_path, '../slike/building_1.jpg'))
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)  # Convert

orb = cv2.ORB_create()
key_points, description = orb.detectAndCompute(img, None)
#img_building_keypoints =cv2.drawKeypoints(img_building,key_points,img_building,flags=cv2.DRAW_MATCHES_FLAGS_DEFAULT)
# rich keypoints: 
img_keypoints =cv2.drawKeypoints(img,key_points,img,flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

#Draw Resultant Image:
plt.plot,plt.imshow(img_keypoints)
plt.title('ORB Interest Point'), plt.xticks([]), plt.yticks([])
plt.show()


```

The found interest points/features are circled in the image above. 
As we can see, some of these points are unique to this scene/building like the points near the top of the two towers. 
However, others like the ones at the top of the tree may not be distinctive.


Now let's see if we can extract the same features from a different image of the same cathedral taken from a different angle.


```
import numpy as np
import pandas as pd
import cv2
import matplotlib.pyplot as plt
import os

# Features Detection: 
dataset_path = '.'
img = cv2.imread(os.path.join(dataset_path, '../slike/building_1.jpg'))
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)  # Convert

orb = cv2.ORB_create()
key_points, description = orb.detectAndCompute(img, None)
#img_building_keypoints =cv2.drawKeypoints(img_building,key_points,img_building,flags=cv2.DRAW_MATCHES_FLAGS_DEFAULT)
# rich keypoints: 
img_keypoints =cv2.drawKeypoints(img,key_points,img,flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

#Feature Extraction:
def image_detect_and_compute(detector, img_name):
    """Detect and compute intetrest points and their descriptors."""
    img = cv2.imread(os.path.join(dataset_path, img_name))
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    kp, des = detector.detectAndCompute(img, None)
    return img, kp, des

def draw_image_matches(detector, img1_name, img2_name, nmatches=20):
    """Draw ORB feature matches of the given two images."""
    img1, kp1, des1 = image_detect_and_compute(detector, img1_name)
    img2, kp2, des2 = image_detect_and_compute(detector, img2_name)

    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
    matches = bf.match(des1, des2)
    matches = sorted(matches, key = lambda x: x.distance)

    img_matches = cv2.drawMatches(img1, kp1, img2, kp2, matches[:nmatches],img2, flags=2)
#Draw Resultant Images:
    plt.subplot(121),plt.imshow(img_keypoints)
    plt.title('ORB Interest Point'), plt.xticks([]), plt.yticks([])
    plt.subplot(122),plt.imshow(img_matches)
    plt.title('Detector'), plt.xticks([]), plt.yticks([])
    plt.show()

orb = cv2.ORB_create()
draw_image_matches(orb, '../slike/building_1.jpg','../slike/building_2.jpg')
```

The result of feature detection is shown in following figure:
<p align="center">
<img src="https://i.ibb.co/pxBTJ6P/orb-features.png" alt="orb-features" border="0"></a>
</p>


While result of feature extraction and matching is shown in the next figure: 
<p align="center">
<img src="https://i.ibb.co/5BMdJjb/orb-detector.png" alt="orb-detector" border="0"></a>
</p>

<h4>Task 5</h4>

Perform ORB feature detection on image `roma_1.jpg` .
Perform matching of images `roma_1.jpg` and `roma_2.jpg`. 
Save result images. 

<hr />

That's all folks! 
See ya next week. 

</xmp>

<body>
<script src="http://strapdownjs.com/v/0.2/strapdown.js"></script>
<script type="text/javascript"
  src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
</script>
<script type="text/x-mathjax-config">MathJax.Hub.Config({ tex2jax: { inlineMath: [['$','$'], ['\\(','\\)']], processEscapes: true } });</script>
</html>
