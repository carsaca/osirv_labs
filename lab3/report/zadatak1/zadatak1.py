import numpy as np
import cv2
import matplotlib.pyplot as plt

def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)

def salt_n_pepper_noise(img, percent=10):
  out = img.astype(np.float32)
  limit = ((float(percent)/2.0)/100.0) * 255.0
  noise = np.random.uniform(0,255, img.shape)
  out[noise<limit] = 0
  out[noise>(255-limit)] = 255
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

def uniform_noise(img, low, high):
  out = img.astype(np.float32)
  noise = np.random.uniform(low,high, img.shape)
  out = out + noise
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)


def show(img):
  cv2.imshow("title", img)
  cv2.waitKey(0)
  cv2.destroyAllWindows()

def showhist(img):
  hist,bins = np.histogram(img.flatten(), bins=256, range=(0,255))
  plt.vlines(np.arange(len(hist)), 0, hist)
  plt.title("Histogram")
  plt.show()

img = cv2.imread('images/baboon.bmp', cv2.IMREAD_GRAYSCALE)


show(img)
showhist(img)

show(gaussian_noise(img, 0, 12))
showhist(gaussian_noise(img, 0, 12))
cv2.imwrite("zad1_gaussian.jpg", gaussian_noise(img, 0, 12))


cv2.imwrite("zad1_salt_n_pepper_5.jpg", salt_n_pepper_noise(img, 5))
show(salt_n_pepper_noise(img,5))
showhist(salt_n_pepper_noise(img, 5))

cv2.imwrite("zad1_salt_n_pepper_10.jpg", salt_n_pepper_noise(img, 10))
show(salt_n_pepper_noise(img,10))
showhist(salt_n_pepper_noise(img, 10))

cv2.imwrite("zad1_salt_n_pepper_15.jpg", salt_n_pepper_noise(img, 15))
show(salt_n_pepper_noise(img,15))
showhist(salt_n_pepper_noise(img, 15))

cv2.imwrite("zad1_salt_n_pepper_20.jpg", salt_n_pepper_noise(img, 20))
show(salt_n_pepper_noise(img,20))
showhist(salt_n_pepper_noise(img, 20))


cv2.imwrite("zad1_uniform_20.jpg", uniform_noise(img, -20, 20))
show(uniform_noise(img, -20, 20))
showhist(uniform_noise(img, -20, 20))

cv2.imwrite("zad1_uniform_40.jpg", uniform_noise(img, -40, 40))
show(uniform_noise(img, -40, 40))
showhist(uniform_noise(img, -40, 40))

cv2.imwrite("zad1_uniform_60.jpg", uniform_noise(img, -60, 60))
show(uniform_noise(img, -60, 60))
showhist(uniform_noise(img, -60, 60))