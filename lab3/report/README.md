## Problem 1
Gaussian

![Gaussian](zadatak1/zad1_gaussian.jpg)
![](zadatak1/histogram_gaus.png)

Salt n pepper 5

![Salt n pepper 5](zadatak1/zad1_salt_n_pepper_5.jpg)
![](zadatak1/histogram_SnP_5.png)

Salt n pepper 10

![Salt n pepper 10](zadatak1/zad1_salt_n_pepper_10.jpg)
![](zadatak1/histogram_SnP_10.png)

Salt n pepper 15

![Salt n pepper 15](zadatak1/zad1_salt_n_pepper_15.jpg)
![](zadatak1/histogram_SnP_15.png)

Salt n pepper 20

![Salt n pepper 20](zadatak1/zad1_salt_n_pepper_20.jpg)
![](zadatak1/histogram_SnP_20.png)

Uniform 20

![Uniform 20](zadatak1/zad1_uniform_20.jpg)
![](zadatak1/histogram_uniform_20.png)

Uniform 40

![Uniform 40](zadatak1/zad1_uniform_40.jpg)
![](zadatak1/histogram_uniform_40.png)

Uniform 60

![Uniform 60](zadatak1/zad1_uniform_60.jpg)
![](zadatak1/histogram_uniform_60.png)

Prigazivanje histograma
```
showhist(salt_n_pepper_noise(img, 5))
```

## Problem 2

Za uklanjanje salt n pepper šuma učinkovitji je median filter. 

Salt n pepper 10

![](zadatak2/zad2_boats_snp10.jpg)

Salt n pepper 10 + median filter

![](zadatak2/zad2_boats_snp10_median3x3.jpg)

Salt n pepper 10 + gausian filter 

![](zadatak2/zad2_boats_snp10_gausian5.jpg)

## Problem 3

### Treshhold

![](zadatak3/zadatak3_1.png)


