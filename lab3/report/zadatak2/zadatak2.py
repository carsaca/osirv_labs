# LV4 - zad 2

import numpy as np
import matplotlib.pyplot as plt
import cv2

def show(img):
  cv2.imshow("title", img)
  cv2.waitKey(0)
  cv2.destroyAllWindows()

def salt_n_pepper_noise(img, percent=10):
  out = img.astype(np.float32)
  limit = ((float(percent)/2.0)/100.0) * 255.0
  noise = np.random.uniform(0,255, img.shape)
  out[noise<limit] = 0
  out[noise>(255-limit)] = 255
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)

def autoBlur(img, name):
    cv2.imwrite("zad2_" + name + ".jpg", img)
    cv2.imwrite("zad2_" + name + "_median3x3.jpg", cv2.medianBlur(img, 3))
    cv2.imwrite("zad2_" + name + "_median7x7.jpg", cv2.medianBlur(img, 7)) 
    cv2.imwrite("zad2_" + name + "_gausian5.jpg", cv2.GaussianBlur(img,(3, 3), 5))
    cv2.imwrite("zad2_" + name + "_gausian15.jpg", cv2.GaussianBlur(img,(3, 3), 15))
    cv2.imwrite("zad2_" + name + "_gausian7x7.jpg", cv2.GaussianBlur(img,(7, 7), 15))

def myMedianBlur(imgage, radius):
    
    output = output.astype(np.uint8)
    return output  


boats = cv2.imread('../../../images/boats.bmp', cv2.IMREAD_GRAYSCALE)
airplane = cv2.imread('../../../images/airplane.bmp', cv2.IMREAD_GRAYSCALE)

boats_snp1 = salt_n_pepper_noise(boats, 1)
boats_snp10 = salt_n_pepper_noise(boats, 10)
boats_g5 = gaussian_noise(boats, 0, 5)
boats_g15 = gaussian_noise(boats, 0, 15)
boats_g35 = gaussian_noise(boats, 0, 35)

airplane_snp1 = salt_n_pepper_noise(airplane, 1)
airplane_snp10 = salt_n_pepper_noise(airplane, 10)
airplane_g5 = gaussian_noise(airplane, 0, 5)
airplane_g15 = gaussian_noise(airplane, 0, 15)
airplane_g35 = gaussian_noise(airplane, 0, 35)

show(cv2.medianBlur(airplane_snp10, 3))

autoBlur(salt_n_pepper_noise(boats, 1),'boats_snp1')
autoBlur(boats_snp10,'boats_snp10')
autoBlur(boats_g5,'boats_g5')
autoBlur(boats_g15,'boats_g15')
autoBlur(boats_g35,'boats_g35')

autoBlur(airplane_snp1,'airplane_snp1')
autoBlur(airplane_snp10, 'airplane_snp10')
autoBlur(airplane_g5, 'airplane_g5')
autoBlur(airplane_g15, 'airplane_g15')
autoBlur(airplane_g35, 'airplane_g35')
