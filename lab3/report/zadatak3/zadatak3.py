# LV4 - zad 3

import cv2
import numpy as np
from matplotlib import pyplot as plt

def show(img):
  cv2.imshow("title", img)
  cv2.waitKey(0)
  cv2.destroyAllWindows()

boats = cv2.imread('../../../images/boats.bmp', cv2.IMREAD_GRAYSCALE)
baboon = cv2.imread('../../../images/baboon.bmp', cv2.IMREAD_GRAYSCALE)
airplane = cv2.imread('../../../images/airplane.bmp', cv2.IMREAD_GRAYSCALE)

def treshould(img):
    ret,thresh1 = cv2.threshold(img,127,255,cv2.THRESH_BINARY)
    ret,thresh2 = cv2.threshold(img,127,255,cv2.THRESH_BINARY_INV)
    ret,thresh3 = cv2.threshold(img,127,255,cv2.THRESH_TRUNC)
    ret,thresh4 = cv2.threshold(img,127,255,cv2.THRESH_TOZERO)
    ret,thresh5 = cv2.threshold(img,127,255,cv2.THRESH_TOZERO_INV)
    thresh6 = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_MEAN_C,\
            cv2.THRESH_BINARY,11,2)
    thresh7 = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
            cv2.THRESH_BINARY,11,2)
    thresh8 = cv2.threshold(img,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

    images = [img, thresh1, thresh2, thresh3, thresh4, thresh5, thresh6, thresh7, thresh8]
    return images
    
titles = ['Original Image','BINARY','BINARY_INV','TRUNC','TOZERO','TOZERO_INV','ADAPTIVE_MEAN','ADAPTIVE_GAUSSIAN','OTSU']

images = treshould(boats)
for i in range(8):
    plt.subplot(3,8,i+1),plt.imshow(images[i],'gray')
    plt.title(titles[i])
    plt.xticks([]),plt.yticks([])

images = treshould(baboon)
for i in range(8):
    plt.subplot(3,8,i+9),plt.imshow(images[i],'gray')
    plt.title(titles[i])
    plt.xticks([]),plt.yticks([])

images = treshould(airplane)
for i in range(8):
    plt.subplot(3,8,i+17),plt.imshow(images[i],'gray')
    plt.title(titles[i])
    plt.xticks([]),plt.yticks([])

plt.show()
