from numpy  import *
import cv2

def show(title, image):
    cv2.imshow(title,image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def stroke(image,size):
    return cv2.copyMakeBorder( image,size,size,size,size,cv2.BORDER_CONSTANT,1)

slika1 = cv2.imread('../../../images/pepper.bmp')
slika2 = cv2.imread('../../../images/airplane.bmp')
slika3 = cv2.imread('../../../images/lenna.bmp')

nova_slika = hstack((slika1, slika2, slika3))

#show("test", stroke(nova_slika, 25))
cv2.imwrite("problem1.jpg", stroke(nova_slika, 25))
