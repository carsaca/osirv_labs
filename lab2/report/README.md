## Problem 1

Spajanje 3 slike s hstack.
```
nova_slika = hstack((slika1, slika2, slika3))
```
![](problem1/problem1.jpg)
Dodavanje okvira.
```
def stroke(image,size):
    return cv2.copyMakeBorder( image,size,size,size,size,cv2.BORDER_CONSTANT,1)
```
![](problem1/problem1.jpg)

## Problem 2

Konvolucija.
Primjer koda za jedan kernel:
```
kernel_1 = np.array([[0, 0, 0], 
                     [0, 1, 0],
                     [0, 0, 0]], dtype = float)
img1 = convolve(slika, kernel_1)
cv2.imwrite('kernel_1.png', img1)
```

## Problem 3

```
path = '../../../images'
filename_extension = "_invert.png"
images = []
inverted_images = []

for filename in os.listdir(path):
    img = cv2.imread(os.path.join(path,filename),cv2.IMREAD_GRAYSCALE)
    if img is not None:
        images.append(img)
        inverted_images.append(invert(img))
        cv2.imwrite("".join((filename[:-4], filename_extension)), invert(img))
```

![](problem3/airplane_invert.png)

## Problem 4


```
def treshold(n):
    for filename in os.listdir(path):
        img = cv2.imread(os.path.join(path,filename))
        if img is not None:

            cv2.imwrite(filename[:-4] + '_' + str(n) + '_thresh.png', clip(img, n, 255, img))

treshold(191)

```
![](problem4/airplane_191_thresh.png)

## Problem 5


```

def quantisation(q, img):
    d = power(2, 8 - q)
    img = img.astype(float32)

    clip(img, 0, 255, img)
    img[:] = (floor(img / d) + 0.5) * d

    img = img.astype(uint8)
    cv2.imwrite('boats_' + str(q) + '.bmp', img)
```
![](problem5/boats_1.bmp)


## Problem 6


```
def quantisation(q, img, noise):
    d = power(2, 8 - q)
    img = img.astype(float32)

    clip(img, 0, 255, img)
    img[:] = (floor(img / d + noise) + 0.5) * d

    img = img.astype(uint8)
    cv2.imwrite('boats_' + str(q) + '.bmp', img)
```
![](problem6/boats_1.bmp)

