import numpy as np
import cv2

def convolve(image, kernel):
    output = np.zeros( (image.shape[0] -kernel.shape[0] + 1,
                        image.shape[1] - kernel.shape[1] +1) )
    kernel_rev = kernel[::-1,::-1]


    for i in range(0, output.shape[0]):
        for j in range(0, output.shape[1]):
            for k in range(kernel.shape[0]):
                for l in range(kernel.shape[1]):
                    output[i,j] += image[i+k,j+l] * kernel_rev[k,l]

    output[output>255] = 255
    output[output<0]   = 0
    output = output.astype(np.uint8)
    return output 

def show(title, image):
    cv2.imshow(title,image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

slika = cv2.imread('../../../images/lenna.bmp')


kernel_1 = np.array([[0, 0, 0], 
                     [0, 1, 0],
                     [0, 0, 0]], dtype = float)

kernel_2 = np.array([[1, 0, -1],
                     [0, 0, 0],
                     [-1, 0, 1]], dtype = float)

kernel_3 = np.array([[0, 1, 0],
                     [1, -4, 1],
                     [0, 1, 0]], dtype = float)

kernel_4 = np.array([[-1, -1, -1],
                     [-1, 8, -1],
                     [-1, -1, -1]], dtype = float)

kernel_5 = np.array([[0, -1, 0],
                     [-1, 5, -1],
                     [0, -1, 0]], dtype = float)

kernel_6 = np.array([[1, 1, 1],
                    [1, 1, 1],
                    [1, 1, 1]], dtype = float)
kernel_6 *= 1/9.0

kernel_7 = np.array([[1, 2, 1],
                     [2, 4, 2],
                     [1, 2, 1]], dtype = float)
kernel_7 *= 1/16.0

kernel_8 = np.array([[1, 4, 6, 4, 1],
                     [4, 16, 24, 16, 4],
                     [6, 24, 36, 24, 6],
                     [4, 16, 24, 16, 4],
                     [1, 4, 6, 4, 1]], dtype = float)
kernel_8 *= 1/256.0

kernel_9 = np.array([[1, 4, 6, 4, 1],
                     [4, 16, 24, 16, 4],
                     [6, 24, -476, 24, 6],
                     [4, 16, 24, 16, 4],
                     [1, 4, 6, 4, 1]], dtype = float)
kernel_9 *= (-1/256.0)



img1 = convolve(slika, kernel_1)
img2 = convolve(slika, kernel_2)
img3 = convolve(slika, kernel_3)
img4 = convolve(slika, kernel_4)
img5 = convolve(slika, kernel_5)
img6 = convolve(slika, kernel_6)
img7 = convolve(slika, kernel_7)
img8 = convolve(slika, kernel_8)
img9 = convolve(slika, kernel_9)

#show("test", convolve(slika, kernel_2))

cv2.imwrite('kernel_1.png', img1)
cv2.imwrite('kernel_2.png', img2)
cv2.imwrite('kernel_3.png', img3)
cv2.imwrite('kernel_4.png', img4)
cv2.imwrite('kernel_5.png', img5)
cv2.imwrite('kernel_6.png', img6)
cv2.imwrite('kernel_7.png', img7)
cv2.imwrite('kernel_8.png', img8)
cv2.imwrite('kernel_9.png', img9)