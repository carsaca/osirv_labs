from numpy  import *
import cv2
import os

def show(title, image):
    cv2.imshow(title,image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

path = '../../../images'
#filename_extension = "_parametervalue_thresh.png"

def treshold(n):
    for filename in os.listdir(path):
        img = cv2.imread(os.path.join(path,filename))
        if img is not None:

            cv2.imwrite(filename[:-4] + '_' + str(n) + '_thresh.png', clip(img, n, 255, img))


treshold(63)
treshold(127)
treshold(191)



