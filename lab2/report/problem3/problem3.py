from numpy  import *
import cv2
import os

def show(title, image):
    cv2.imshow(title,image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


path = '../../../images'
filename_extension = "_invert.png"
images = []
inverted_images = []

for filename in os.listdir(path):
    img = cv2.imread(os.path.join(path,filename),cv2.IMREAD_GRAYSCALE)
    if img is not None:
        images.append(img)
        inverted_images.append(invert(img))
        cv2.imwrite("".join((filename[:-4], filename_extension)), invert(img))

