from numpy  import *
import cv2

def quantisation(q, img):
    d = power(2, 8 - q)
    img = img.astype(float32)

    clip(img, 0, 255, img)
    img[:] = (floor(img / d) + 0.5) * d

    img = img.astype(uint8)
    cv2.imwrite('boats_' + str(q) + '.bmp', img)

    #cv2.imshow("test", img)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()


img = cv2.imread("../../../images/BoatsColor.bmp", cv2.IMREAD_GRAYSCALE)


for i in range(1, 9):
    quantisation(i, img)
