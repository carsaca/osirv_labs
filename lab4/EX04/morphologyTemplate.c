#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


/*--------------------------------------------------------------------------*/
/*                                                                          */
/*           MORPHOLOGY WITH A SQUARE-SHAPED STRUCTURING ELEMENT            */
/*                                                                          */
/*                 (Copyright by Joachim Weickert, 11/2000)                 */
/*                                                                          */
/*--------------------------------------------------------------------------*/


/* 
 Osher-Sethian scheme (explicit, one-sided differences) 
 Satisfies extremum principle for time steps ht <= 0.5. 
*/


/*--------------------------------------------------------------------------*/

void alloc_vector

     (float **vector,   /* vector */
      long  n)          /* size */

     /* allocates storage for a vector of size n */


{
*vector = (float *) malloc (n * sizeof(float));
if (*vector == NULL)
   {
   printf("alloc_vector: not enough storage available\n");
   exit(1);
   }
return;
}

/*--------------------------------------------------------------------------*/

void alloc_matrix

     (float ***matrix,  /* matrix */
      long  nx,         /* size in x direction */
      long  ny)         /* size in y direction */

     /* allocates storage for matrix of size nx * ny */


{
long i;

*matrix = (float **) malloc (nx * sizeof(float *));
if (*matrix == NULL)
   {
   printf("alloc_matrix: not enough storage available\n");
   exit(1);
   }
for (i=0; i<nx; i++)
    {
    (*matrix)[i] = (float *) malloc (ny * sizeof(float));
    if ((*matrix)[i] == NULL)
       {
       printf("alloc_matrix: not enough storage available\n");
       exit(1);
       }
    }
return;
}

/*--------------------------------------------------------------------------*/

void disalloc_vector

     (float *vector,    /* vector */
      long  n)          /* size */

     /* disallocates storage for a vector of size n */

{
free(vector);
return;
}

/*--------------------------------------------------------------------------*/

void disalloc_matrix

     (float **matrix,   /* matrix */
      long  nx,         /* size in x direction */
      long  ny)         /* size in y direction */

     /* disallocates storage for matrix of size nx * ny */

{
long i;
for (i=0; i<nx; i++)
    free(matrix[i]);
free(matrix);
return;
}

/*--------------------------------------------------------------------------*/

void dilation 

     (long     nx,        /* image dimension in x direction */
      long     ny,        /* image dimension in y direction */
      long     m,         /* size of structuring element: (2m+1)*(2m+1) */
      float    **u)       /* input: original image; output: processed */

/*
 Dilation with a square of size (2m + 1) * (2m + 1) as structuring element.
*/

{
long    i, j, k;    /* loop variables */
long    n;          /* max(nx,ny) */
float   max;        /* time saver */
float   *f;         /* auxiliary vector */


/* ---- allocate storage ---- */

if (nx > ny) n = nx; else n = ny;
alloc_vector (&f, n+2*m+1);


/* ---- dilation in x direction ---- */

for (j=1; j<=ny; j++)
    {
    /* copy row in vector with reflected boundary layer */
    for (i=1; i<=m; i++)
        f[i] = u[m+1-i][j];
    for (i=1; i<=nx; i++)
        f[m+i] = u[i][j];
    for (i=1; i<=m; i++)
        f[m+nx+i] = u[nx-i+1][j];
 
    /* perform dilation for each pixel */
    for (i=1; i<=nx; i++)  
        {
        max = f[i];
        for (k=i+1; k<=i+2*m; k++)
            if (f[k] > max) max = f[k];
        u[i][j] = max; 
        }
    }


/* ---- dilation in y direction ---- */

for (i=1; i<=nx; i++)
    {
    /* copy column in vector with refelcted boundary layer */
    for (j=1; j<=m; j++)
        f[j] = u[i][m+1-j];
    for (j=1; j<=ny; j++)
        f[m+j] = u[i][j];
    for (j=1; j<=m; j++)
        f[m+ny+j] = u[i][ny-j+1];

    /* perform dilation for each pixel */
    for (j=1; j<=ny; j++) 
        {
        max = f[j];
        for (k=j+1; k<=j+2*m; k++)
            if (f[k] > max) max = f[k];
        u[i][j] = max;
        }
    }


/* ---- disallocate storage for f ---- */

disalloc_vector (f, n+2*m+1);
return;

} /* dilation */

/*--------------------------------------------------------------------------*/

void erosion 

     (long     nx,        /* image dimension in x direction */
      long     ny,        /* image dimension in y direction */
      long     m,         /* size of structuring element: (2m+1)*(2m+1) */
      float    **u)       /* input: original image; output: processed */

/*
 Erosion with a square of size (2m + 1) * (2m + 1) as structuring element.
*/

{
long    i, j, k;    /* loop variables */
long    n;          /* max(nx,ny) */
float   min;        /* time saver */
float   *f;         /* auxiliary vector */


/* ---- allocate storage ---- */

if (nx > ny) n = nx; else n = ny;
alloc_vector (&f, n+2*m+1);


/* ---- erosion in x direction ---- */

for (j=1; j<=ny; j++)
    {
    /* copy row in vector with reflected boundary layer */
    for (i=1; i<=m; i++)
        f[i] = u[m+1-i][j];
    for (i=1; i<=nx; i++)
        f[m+i] = u[i][j];
    for (i=1; i<=m; i++)
        f[m+nx+i] = u[nx-i+1][j];
 
    /* perform erosion for each pixel */
    for (i=1; i<=nx; i++)  
        {
        min = f[i];
        for (k=i+1; k<=i+2*m; k++)
            if (f[k] < min) min = f[k];
        u[i][j] = min; 
        }
    }


/* ---- erosion in y direction ---- */

for (i=1; i<=nx; i++)
    {
    /* copy column in vector with refelcted boundary layer */
    for (j=1; j<=m; j++)
        f[j] = u[i][m+1-j];
    for (j=1; j<=ny; j++)
        f[m+j] = u[i][j];
    for (j=1; j<=m; j++)
        f[m+ny+j] = u[i][ny-j+1];

    /* perform dilation for each pixel */
    for (j=1; j<=ny; j++) 
        {
        min = f[j];
        for (k=j+1; k<=j+2*m; k++)
            if (f[k] < min) min = f[k];
        u[i][j] = min;
        }
    }


/* ---- disallocate storage for f ---- */

disalloc_vector (f, n+2*m+1);
return;

} /* erosion */

/*--------------------------------------------------------------------------*/

void closing 

     (long     nx,        /* image dimension in x direction */
      long     ny,        /* image dimension in y direction */
      long     m,         /* size of structuring element: (2m+1)*(2m+1) */
      float    **u)       /* input: original image; output: processed */

/*
 Closing with a square of size (2m + 1) * (2m + 1) as structuring element.
*/

{
    // gcc morp..c -o run -lm
    // ./run

dilation (nx, ny, m, u);
erosion (nx, ny, m, u);

return;
} 

/*--------------------------------------------------------------------------*/

void opening 

     (long     nx,        /* image dimension in x direction */
      long     ny,        /* image dimension in y direction */
      long     m,         /* size of structuring element: (2m+1)*(2m+1) */
      float    **u)       /* input: original image; output: processed */

/*
 Opening with a square of size (2m + 1) * (2m + 1) as structuring element.
*/

{

erosion (nx, ny, m, u);
dilation (nx, ny, m, u);

return;
} 

/*--------------------------------------------------------------------------*/

void white_top_hat 

     (long     nx,        /* image dimension in x direction */
      long     ny,        /* image dimension in y direction */
      long     m,         /* size of structuring element: (2m+1)*(2m+1) */
      float    **u)       /* input: original image; output: processed */

/*
 White top hat with a square of size (2m + 1) * (2m + 1) as structuring 
 element.
*/

{
long    i, j;       /* loop variables */
float   **uo;       /* opening of input image */

alloc_matrix (&uo, nx+2, ny+2);

for (i=1; i<=nx; i++){
    for (j=1; j<=ny; j++){
        uo[i][j]=u[i][j];
    }
};
opening (nx, ny, m, uo);

//slika - slika.closing
for (i=1; i<=nx; i++){
    for (j=1; j<=ny; j++){
        u[i][j]=u[i][j] - uo[i][j];
    }
}


disalloc_matrix (uo, nx+2, ny+2);
return;

} /* white_top_hat */ 

/*--------------------------------------------------------------------------*/

void black_top_hat 

     (long     nx,        /* image dimension in x direction */
      long     ny,        /* image dimension in y direction */
      long     m,         /* size of structuring element: (2m+1)*(2m+1) */
      float    **u)       /* input: original image; output: processed */

/*
 Black top hat with a square of size (2m + 1) * (2m + 1) as structuring 
 element.
*/

{
long    i, j;       /* loop variables */
float   **uc;       /* closing of input image */

alloc_matrix (&uc, nx+2, ny+2);

for (i=1; i<=nx; i++){
    for (j=1; j<=ny; j++){
        uc[i][j]=u[i][j];
    }
}
closing (nx, ny, m, uc);

//slika - slika.closing
for (i=1; i<=nx; i++){
    for (j=1; j<=ny; j++){
        u[i][j]=uc[i][j] - u[i][j];
    }
}

disalloc_matrix (uc, nx+2, ny+2);
return;

} /* black_top_hat */ 

/*--------------------------------------------------------------------------*/

void selfdual_top_hat 

     (long     nx,        /* image dimension in x direction */
      long     ny,        /* image dimension in y direction */
      long     m,         /* size of structuring element: (2m+1)*(2m+1) */
      float    **u)       /* input: original image; output: processed */

/*
 Black top hat with a square of size (2m + 1) * (2m + 1) as structuring 
 element.
*/

{
long    i, j;       /* loop variables */
float   **uo;       /* copy of input image */

alloc_matrix (&uo, nx+2, ny+2);

for (i=1; i<=nx; i++){
    for (j=1; j<=ny; j++){
        uo[i][j]=u[i][j];
    }
}
opening (nx, ny, m, uo);
closing (nx, ny, m, u);
//slika - slika.closing
for (i=1; i<=nx; i++){
    for (j=1; j<=ny; j++){
        u[i][j]=u[i][j] - uo[i][j];
    }
}

disalloc_matrix (uo, nx+2, ny+2);
return;

} /* selfdual_top_hat */ 

/*--------------------------------------------------------------------------*/

void contrast_enhancement 

     (long     nx,        /* image dimension in x direction */
      long     ny,        /* image dimension in y direction */
      long     m,         /* size of structuring element: (2m+1)*(2m+1) */
      float    factor,    /* enhancement factor; 0 gives no enhancement */
      float    **u)       /* input: original image; output: processed */

/*
 Contrast enhancement using black and white top hats. 
*/

{
long    i, j;         /* loop variables */
float   **uw, **ub;   /* white and black top hats */

alloc_matrix (&uw, nx+2, ny+2);
alloc_matrix (&ub, nx+2, ny+2);

for (i=1; i<=nx; i++){
    for (j=1; j<=ny; j++){
        uw[i][j]=u[i][j];
        ub[i][j]=u[i][j];
    }
}
white_top_hat(nx,ny,m,uw);
black_top_hat(nx,ny,m,ub);

for (i=1; i<=nx; i++){
    for (j=1; j<=ny; j++){
        u[i][j]=factor * uw[i][j] + u[i][j] - factor * ub[i][j];
    }
}

disalloc_matrix (uw, nx+2, ny+2);
disalloc_matrix (ub, nx+2, ny+2);

} /* contrast_enhancement */

/*--------------------------------------------------------------------------*/

void analyse

     (float   **u,         /* image, unchanged */
      long    nx,          /* pixel number in x direction */
      long    ny,          /* pixel number in x direction */
      float   *min,        /* minimum, output */
      float   *max,        /* maximum, output */
      float   *mean,       /* mean, output */
      float   *vari)       /* variance, output */

/*
 calculates minimum, maximum, mean and variance of an image u
*/

{
long    i, j;       /* loop variables */
float   help;       /* auxiliary variable */
double  help2;      /* auxiliary variable */

*min  = u[1][1];
*max  = u[1][1];
help2 = 0.0;
for (i=1; i<=nx; i++)
 for (j=1; j<=ny; j++)
     {
     if (u[i][j] < *min) *min = u[i][j];
     if (u[i][j] > *max) *max = u[i][j];
     help2 = help2 + (double)u[i][j];
     }
*mean = (float)help2 / (nx * ny);

*vari = 0.0;
for (i=1; i<=nx; i++)
 for (j=1; j<=ny; j++)
     {
     help  = u[i][j] - *mean;
     *vari = *vari + help * help;
     }
*vari = *vari / (nx * ny);

return;

} /* analyse */

/*--------------------------------------------------------------------------*/

int main ()

{
char   row[80];              /* for reading data */
char   in[80];               /* for reading data */
char   out[80];              /* for reading data */
float  **u;                  /* image */
long   i, j;                 /* loop variable */ 
long   nx, ny;               /* image size in x, y direction */ 
FILE   *inimage, *outimage;  /* input file, output file */
long   goal;                 /* largest iteration number */
long   m;                    /* size of structuring element: (2m+1)*(2m+1) */
float  factor;               /* enhancement factor */
float  max, min;             /* largest, smallest grey value */
float  mean;                 /* average grey value */
float  vari;                 /* variance */
unsigned char byte;          /* for data conversion */

printf("\n");
printf("MORPHOLOGY WITH SQUARE-SHAPED STRUCTURING ELEMENT\n\n");
printf("*************************************************\n\n");
printf("    Copyright 2000 by Joachim Weickert       \n");
printf("    Dept. of Mathematics and Computer Science\n");
printf("    University of Mannheim, Germany          \n\n");
printf("    All rights reserved. Unauthorized usage, \n");
printf("    copying, hiring, and selling prohibited. \n\n");
printf("    Send bug reports to                      \n");
printf("    Joachim.Weickert@uni-mannheim.de         \n\n");
printf("*************************************************\n\n");

/* ---- read input image (pgm format P5) ---- */

/* read image name */
printf("input image:                      ");
gets (in);

/* open pgm file and read header */
inimage = fopen(in,"r");
fgets (row, 80, inimage);
fgets (row, 80, inimage);
while (row[0]=='#') fgets(row, 80, inimage);
sscanf (row, "%ld %ld", &nx, &ny);
fgets (row, 80, inimage);

/* allocate storage */
alloc_matrix (&u, nx+2, ny+2);

/* read image data */
for (j=1; j<=ny; j++)
 for (i=1; i<=nx; i++)
     u[i][j] = (float) getc (inimage);
fclose(inimage);


/* ---- read other parameters ---- */

printf("goal:                             \n");
printf("  (0) dilation                    \n");
printf("  (1) erosion                     \n");
printf("  (2) closing                     \n");
printf("  (3) opening                     \n");
printf("  (4) white top hat               \n");
printf("  (5) black top hat               \n");
printf("  (6) selfdual top hat            \n");
printf("  (7) contrast enhancement        \n");
printf("your choice:                      ");
gets(row);  sscanf(row, "%ld", &goal);
printf("size m of (2m+1) * (2m+1) mask:   ");
gets(row);  sscanf(row, "%ld", &m);
if (goal == 7)
   {
   printf("enhancement factor (>0)           ");
   gets(row);  sscanf(row, "%f", &factor);
   }
printf("output image:                     ");
gets(out);
printf("\n");


/* ---- process image ---- */

if (goal == 0) 
   dilation (nx, ny, m, u);
if (goal == 1) 
   erosion (nx, ny, m, u);
if (goal == 2)  
   closing (nx, ny, m, u);
if (goal == 3)  
   opening (nx, ny, m, u);
if (goal == 4)  
   white_top_hat (nx, ny, m, u);
if (goal == 5)  
   black_top_hat (nx, ny, m, u);
if (goal == 6)  
   selfdual_top_hat (nx, ny, m, u);
if (goal == 7)  
   contrast_enhancement (nx, ny, m, factor, u);

/* ---- check minimum, maximum, mean, variance ---- */

analyse (u, nx, ny, &min, &max, &mean, &vari);
printf("minimum:       %8.2f \n", min);
printf("maximum:       %8.2f \n", max);
printf("mean:          %8.2f \n", mean);
printf("variance:      %8.2f \n\n", vari);


/* ---- write output image (pgm format P5) ---- */

/* open file and write header (incl. filter parameters) */
outimage = fopen (out, "w");
fprintf (outimage, "P5 \n");
if (goal == 0) fprintf (outimage, "# dilation\n");
if (goal == 1) fprintf (outimage, "# erosion\n");
if (goal == 2) fprintf (outimage, "# closing\n");
if (goal == 3) fprintf (outimage, "# opening\n");
if (goal == 4) fprintf (outimage, "# white top hat\n");
if (goal == 5) fprintf (outimage, "# black top hat\n");
if (goal == 6) fprintf (outimage, "# selfdual top hat\n");
if (goal == 7) fprintf (outimage, "# contrast enhancement\n");
fprintf (outimage, "# initial image:   %s\n", in);
fprintf (outimage, "# struct. element: square\n");
fprintf (outimage, "# size:            %1ld * %1ld\n", 2*m+1, 2*m+1);
if (goal == 7) fprintf (outimage, "# enhancement:     %4.2f\n", factor);
fprintf (outimage, "%ld %ld \n255\n",  nx, ny);

/* write image data and close file */
for (j=1; j<=ny; j++)
 for (i=1; i<=nx; i++)
     {
     if (u[i][j] < 0.0)
        byte = (unsigned char)(0.0);
     else if (u[i][j] > 255.0)
        byte = (unsigned char)(255.0);
     else
        byte = (unsigned char)(u[i][j]);
     fwrite (&byte, sizeof(unsigned char), 1, outimage);
     }
fclose(outimage);
printf("output image %s successfully written\n\n", out);


/* ---- disallocate storage ---- */

disalloc_matrix (u, nx+2, ny+2);
return(0);
}
