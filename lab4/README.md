## Problem 1

Closing

```
dilation (nx, ny, m, u);
erosion (nx, ny, m, u);
```

Opening

```
erosion (nx, ny, m, u);
dilation (nx, ny, m, u);
```

White top hat

```
for (i=1; i<=nx; i++){
    for (j=1; j<=ny; j++){
        uo[i][j]=u[i][j];
    }
};
opening (nx, ny, m, uo);

for (i=1; i<=nx; i++){
    for (j=1; j<=ny; j++){
        u[i][j]=u[i][j] - uo[i][j];
    }
}
```

Black top hat

```
for (i=1; i<=nx; i++){
    for (j=1; j<=ny; j++){
        uc[i][j]=u[i][j];
    }
}
closing (nx, ny, m, uc);

for (i=1; i<=nx; i++){
    for (j=1; j<=ny; j++){
        u[i][j]=uc[i][j] - u[i][j];
    }
}
```
Selfdual top hat

```
for (i=1; i<=nx; i++){
    for (j=1; j<=ny; j++){
        uo[i][j]=u[i][j];
    }
}
opening (nx, ny, m, uo);
closing (nx, ny, m, u);

for (i=1; i<=nx; i++){
    for (j=1; j<=ny; j++){
        u[i][j]=u[i][j] - uo[i][j];
    }
}
```
Contrast enhancement

```
for (i=1; i<=nx; i++){
    for (j=1; j<=ny; j++){
        uw[i][j]=u[i][j];
        ub[i][j]=u[i][j];
    }
}
white_top_hat(nx,ny,m,uw);
black_top_hat(nx,ny,m,ub);

for (i=1; i<=nx; i++){
    for (j=1; j<=ny; j++){
        u[i][j]=factor * uw[i][j] + u[i][j] - factor * ub[i][j];
    }
}
```

![](EX04/house_without.pgm)
![](EX04/mammogram_7-4.pgm)
![](EX04/owl_wth.pgm)
![](EX04/fabric_wth.pgm)

Slike se ne žele prikazat.










