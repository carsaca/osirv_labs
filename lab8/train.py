import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data

def build_affine(x):
	W = tf.Variable(tf.zeros([784, 10]))
	b = tf.Variable(tf.zeros([10]))
	logits = tf.matmul(x, W) + b
	return logits

def build_convnet(x):
	x = tf.reshape(x, [-1, 28, 28, 1])
	x = tf.layers.conv2d(x, 32, 5, padding='SAME')
	x = tf.nn.relu(x)
	x = tf.layers.max_pooling2d(x, 2, 2)
	
	x = tf.layers.conv2d(x, 64, 5, padding='SAME')
	x = tf.nn.relu(x)
	x = tf.layers.max_pooling2d(x, 2, 2)
	
	x = tf.reshape(x, [-1, 7*7*64])
	logits = tf.layers.dense(x, 10)
    return logits

mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)

# `x` is a batch of input images
x = tf.placeholder(tf.float32, [None, 784])

logits = build_affine(x)

labels = tf.placeholder(tf.float32, [None, 10])
loss = tf.nn.softmax_cross_entropy_with_logits(labels=labels, logits=logits)
step = tf.train.GradientDescentOptimizer(1e-4).minimize(loss)

# prepare the accuracy-computation graph
correct_prediction = tf.equal(tf.argmax(logits, 1), tf.argmax(labels, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

sess = tf.InteractiveSession()
tf.global_variables_initializer().run()

for k in range(5000):
	batch_xs, batch_labels = mnist.train.next_batch(100)
	sess.run(step, feed_dict={x: batch_xs, labels: batch_labels})
	if k % 200 == 0:
		acc = 100*sess.run(accuracy, feed_dict={x: mnist.test.images, labels: mnist.test.labels})
		print('* iter %d: test set accuracy=%.2f %%' % (k, acc))

print('* Done!')